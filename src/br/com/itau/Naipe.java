package br.com.itau;

public enum Naipe {

    COPAS(1), PAUS(2), OUROS(3), ESPADAS(4);

    private int valor;

    Naipe(int valor) {
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }
}
